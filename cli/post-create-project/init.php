<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

$app = require __DIR__ . '/../../private/app/bootstrap.php';

echo "\r\n";
echo '────────────────────────────────────────────────────────────────────────────────' . "\r\n";
echo "\r\n";

passthru('php ' . realpath(__DIR__ . '/..') . '/vitya.php install');

echo "\r\n";
echo '────────────────────────────────────────────────────────────────────────────────' . "\r\n";
echo "\r\n";

echo "\033[45m\033[37;1m" . '                                                                                ' . "\033[0m" . "\r\n";
echo "\033[45m\033[37;1m" . '  Your Vitya application is ready!                                              ' . "\033[0m" . "\r\n";
echo "\033[45m\033[37;1m" . '                                                                                ' . "\033[0m" . "\r\n";
echo "\r\n";
echo '  You can start using it right now with PHP built-in web server. Type the ' . "\r\n";
echo '  following command in your terminal:' . "\r\n";
echo "\r\n";
echo '  ' . 'php ' . realpath(__DIR__ . '/..') . '/server.php' . "\r\n";
echo "\r\n";
