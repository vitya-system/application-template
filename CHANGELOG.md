# Changelog

## [1.0.0-alpha13] - 2022-06-03
### Changed
- Update Vitya system to alpha9.

## [1.0.0-alpha12] - 2024-09-30
### Changed
- Make the default app a bit more sophisticated.

## [1.0.0-alpha11] - 2024-07-17
### Changed
- Simplify AppController.
- Adapt homepage controller to changes in the framework.
- Modifiy PHP executables handling.

## [1.0.0-alpha10] - 2022-07-01
### Changed
- Update Vitya system to alpha10.
- Simplify app file structure.

## [1.0.0-alpha9] - 2022-06-03
### Changed
- Update Vitya system to alpha9.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Update Vitya system to alpha8.

## [1.0.0-alpha7] - 2021-07-05
### Changed
- Bumped version to alpha7 to match the rest of the system.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Bumped version to alpha6 to match the rest of the system.

## [1.0.0-alpha5] - 2021-05-03
### Changed
- Bumped version to alpha5 to match the rest of the system.

## [1.0.0-alpha4] - 2021-04-07
### Changed
- Bumped version to alpha4 to match the rest of the system.

## [1.0.0-alpha3] - 2021-03-01
### Changed
- Updated PHP dependencies.

## [1.0.0-alpha2] - 2021-02-01
### Changed
- Exclude node_modules from versioning.

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
