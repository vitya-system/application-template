# Vitya PHP application template

This package contains a basic web application template. Use it as a starting
point to create an app based on the Vitya framework.

## How to setup a new application?

You can use Composer to create your app:

```
composer create-project vitya/application-template my-new-project -s alpha
```

(NB: The -s alpha option is necessary for now, since there is no stable release
yet.)

Then you can launch your new app using PHP built-in web server:

```
php my-new-project/cli/server.php
```

By default, your application will be available at [http://localhost:8000](http://localhost:8000),
but you can specify another hostname or port number. For example, this will
make your website available to other machines on your network, on port 1234:

```
php my-new-project/cli/server.php 0.0.0.0:1234
```

WARNING: the built-in web server is only intended to be used as a development
tool! Do NOT use it in a production environment!
