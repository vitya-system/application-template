<?php

declare(strict_types=1);

namespace App;

use Psr\Http\Message\ResponseInterface;
use Vitya\Application\Controller\AbstractController;

class AppController extends AbstractController
{
    public function homepage(): ResponseInterface
    {
        $html = '<!DOCTYPE html>
            <html lang="en">
                <head>
                    <title>Homepage</title>
                </head>
                <body>
                    <h1>Homepage</h1>
                    <p>This is a Vitya application.</p>
                    <p><a href="' . htmlspecialchars((string) $this->uri('another-page')) . '">Go to another page</a></p>
                </body>
            </html>
        ';
        return $this->html($html);
    }

    public function anotherPage(): ResponseInterface
    {
        $html = '<!DOCTYPE html>
            <html lang="en">
                <head>
                    <title>Another page</title>
                </head>
                <body>
                    <h1>Another page</h1>
                    <p>This is another page in a Vitya application.</p>
                    <p><a href="' . htmlspecialchars((string) $this->uri('homepage')) . '">Go back to the homepage</a></p>
                </body>
            </html>
        ';
        return $this->html($html);
    }

}
